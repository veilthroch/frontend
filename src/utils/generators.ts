function ord(a: number, b: number, c: number): boolean {
    return Math.sign(a - b) === Math.sign(b - c);
}

/**
 * Returns an number iterator from start to stop
 * @param start - first number yielded
 * @param stop - first number not yielded
 * @param step - difference between successive numbers
 */
// 'outsourced' for better implementation from https://codereview.stackexchange.com/questions/115704/
export function* range(start: number, stop: number | null = null, step: number | null = null) {
    if (stop === null) {
        [start, stop, step] = [0, start, Math.sign(start)];
    } else if (step === null) {
        step = Math.sign(stop - start) || 1;
    } else if (Math.sign(stop - start) != Math.sign(step)) {
        return;
    }
    if (start === stop) return;

    let i = 0,
        result;
    do {
        result = start + i * step;
        yield result;
        ++i;
    } while (ord(start, start + i * step, stop));
}

export function groupBy<T, K>(list: Iterable<T>, keyGetter: (item: T) => K): Map<K, T[]> {
    const map = new Map<K, T[]>();

    for (let item of list) {
        const key = keyGetter(item);
        const collection = map.get(key);

        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    }

    return map;
}
