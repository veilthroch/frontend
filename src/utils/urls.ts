import urlJoin from "url-join";

export function url(...components: string[]): string {
    return urlJoin(process.env.VUE_APP_BASE_URL, ...components);
}

export function apiUrl(...components: string[]): string {
    return urlJoin(process.env.VUE_APP_API_URL, ...components);
}
