import { Store } from "vuex";
import { useModule } from "vuex-simple";

export function ensureModule<T extends object>(vuexStore: Store<any>, namespace?: string[]): T {
    let module = useModule<T>(vuexStore, namespace);
    if (module === undefined) throw new Error("Failed to load module");

    return module;
}
