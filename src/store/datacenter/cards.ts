import { Card } from "@/models/Card";
import { apiUrl } from "@/utils/urls";
import { transformAndValidate } from "class-transformer-validator";
import axios from "axios";
import { Action, Getter, Mutation, State } from "vuex-simple";
import { CardSet } from "@/models/CardSet";
import { groupBy } from "@/utils/generators";

export class CardsStore {
    @State() cards: Card[] = [];
    @State() sets: CardSet[] = [];

    @Getter()
    get cardsById(): Map<number, Card> {
        return new Map(this.cards.map(card => [card.id, card]));
    }

    @Getter()
    get categories(): Map<number, Card[]> {
        return groupBy(this.cards, card => card.category);
    }

    @Mutation()
    addCard(card: Card) {
        if (this.cardsById.has(card.id)) return;

        this.cards.push(card);
    }

    @Mutation()
    setCards(cards: Card[]) {
        this.cards = cards;
    }

    @Mutation()
    setSets(sets: CardSet[]) {
        this.sets = sets;
    }

    @Action()
    async fetchCards(): Promise<Card[]> {
        let response = await axios.get(apiUrl("datacenter/kr/patch/87/card/all"));
        let cards = (await transformAndValidate(Card, response.data)) as Card[];

        this.setCards(cards);
        return cards;
    }

    @Action()
    async fetchSets() {
        let response = await axios.get(apiUrl("datacenter/kr/patch/87/card/combines"));
        let sets = (await transformAndValidate(CardSet, response.data)) as CardSet[];

        this.setSets(sets);
        return sets;
    }

    @Action()
    async fetchCard({ id }: DcKey) {
        let response = await axios.get(apiUrl(`datacenter/kr/patch/87/card/${id}`));
        let card = (await transformAndValidate(Card, response.data)) as Card;

        this.addCard(card);
    }
}

interface DcKey {
    id: number;
}
