import Vuex from "vuex";
import { CardsStore } from "@/store/datacenter/cards";
import { Module } from "vuex-simple";

export class DatacenterStore {
    @Module() public cards = new CardsStore();
}
