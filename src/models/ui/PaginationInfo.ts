export class PaginationInfo {
    current: number;
    total: number;

    constructor(current: number, total: number) {
        this.current = current;
        this.total = total;
    }
}
