import { IsArray, IsIn, IsInt, IsOptional, IsPositive, IsString, ValidateNested } from "class-validator";
import { Passivity } from "@/models/Passivity";

export class CardSetEffect {
    @ValidateNested()
    passivity: Passivity;

    @IsInt()
    allCardMinLevel: number;

    constructor(passivity: Passivity, allCardMinLevel: number) {
        this.passivity = passivity;
        this.allCardMinLevel = allCardMinLevel;
    }
}

export class CardSet {
    @IsInt()
    id: number;

    @IsArray()
    @IsInt({ each: true })
    categories: number[];

    @IsArray()
    @ValidateNested()
    effects: CardSetEffect[];

    constructor(id: number, categories: number[], effects: CardSetEffect[]) {
        this.id = id;
        this.categories = categories;
        this.effects = effects;
    }
}
