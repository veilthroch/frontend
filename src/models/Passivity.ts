import {
    IsArray,
    IsBoolean,
    IsIn,
    IsInt,
    IsNumber,
    IsOptional,
    IsPositive,
    IsString,
    ValidateNested,
} from "class-validator";

export class Passivity {
    @IsInt()
    id: number;

    @IsOptional()
    @IsString()
    icon: string | null;

    @IsString()
    @IsOptional()
    internalName: string | null;

    @IsString()
    @IsOptional()
    tooltip: string | null = null;

    @IsString()
    @IsOptional()
    generatedTooltip: string | null = null;

    @IsBoolean()
    isHidden: boolean;

    @IsInt()
    kind: number;

    @IsInt()
    method: number;

    @IsOptional()
    @IsString()
    name: string | null;

    @IsNumber()
    probability: number;

    @IsInt()
    tickInterval: number;

    @IsInt()
    type: number;

    value: number | string;

    constructor(
        id: number,
        icon: string | null,
        internalName: string | null,
        isHidden: boolean,
        kind: number,
        method: number,
        name: string | null,
        probability: number,
        tickInterval: number,
        type: number,
        value: number
    ) {
        this.id = id;
        this.icon = icon;
        this.internalName = internalName;
        this.isHidden = isHidden;
        this.kind = kind;
        this.method = method;
        this.name = name;
        this.probability = probability;
        this.tickInterval = tickInterval;
        this.type = type;
        this.value = value;
    }
}
