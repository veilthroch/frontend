import { IsArray, IsIn, IsInt, IsOptional, IsPositive, IsString, ValidateNested } from "class-validator";
import { Passivity } from "@/models/Passivity";

function normalize(text: string): string {
    const div = document.createElement("div");

    div.innerHTML = text;
    text = div.innerText;
    div.remove();

    return text.replace(" ", "").toLowerCase();
}

export class Card {
    @IsInt()
    id: number;

    @IsInt()
    category: number;

    @IsInt()
    level: number;

    @IsInt()
    property: number;

    @IsString()
    effect: string | null;

    @IsInt()
    itemsNeeded: number;

    @IsPositive()
    cost: number;

    @IsString()
    image: string;

    @IsOptional()
    @ValidateNested()
    superiorCard: Card | null;

    @IsInt()
    exp: number;

    @IsString()
    name: string;

    @IsString()
    source: string;

    @IsString()
    lore: string;

    @IsArray()
    @ValidateNested({ each: true })
    passivities: Passivity[];

    matchesSearch(text: string): boolean {
        let stripped = normalize(text);

        if (stripped.match(`(cost:)?${this.cost}`)) return true;
        if (normalize(this.name).includes(stripped)) return true;

        for (let passivity of this.passivities) {
            if (normalize(passivity.generatedTooltip || "").includes(stripped)) return true;
        }

        return false;
    }

    constructor(
        id: number,
        category: number,
        level: number,
        property: number,
        effect: string,
        itemsNeeded: number,
        cost: number,
        image: string,
        superiorCard: Card,
        exp: number,
        name: string,
        source: string,
        lore: string,
        passivities: Passivity[]
    ) {
        this.id = id;
        this.category = category;
        this.level = level;
        this.property = property;
        this.effect = effect;
        this.itemsNeeded = itemsNeeded;
        this.cost = cost;
        this.image = image;
        this.superiorCard = superiorCard;
        this.exp = exp;
        this.name = name;
        this.source = source;
        this.lore = lore;
        this.passivities = passivities;
    }
}
