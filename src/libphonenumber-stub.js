// workaround from here https://github.com/typestack/class-validator/pull/258#issuecomment-521587401
// class-validator has HUUUGE dependency this will throw out

module.exports = {
    PhoneNumberUtil: {
        getInstance() {
            return null;
        },
    },
};
