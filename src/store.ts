import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import { DatacenterStore } from "@/store/datacenter";
import { createVuexStore, Module } from "vuex-simple";

export class RootStore {
    @Module() datacenter = new DatacenterStore();
}

export const rootStore: RootStore = new RootStore();
export default createVuexStore(rootStore);
