const entryPrefix = "./src/";
const path = require("path");
const urlJoin = require("url-join");

function enableShadowCss(config) {
    const configs = [
        config.module.rule("vue").use("vue-loader"),
        config.module
            .rule("css")
            .oneOf("vue-modules")
            .use("vue-style-loader"),
        config.module
            .rule("css")
            .oneOf("vue")
            .use("vue-style-loader"),
        config.module
            .rule("css")
            .oneOf("normal-modules")
            .use("vue-style-loader"),
        config.module
            .rule("css")
            .oneOf("normal")
            .use("vue-style-loader"),
        config.module
            .rule("postcss")
            .oneOf("vue-modules")
            .use("vue-style-loader"),
        config.module
            .rule("postcss")
            .oneOf("vue")
            .use("vue-style-loader"),
        config.module
            .rule("postcss")
            .oneOf("normal-modules")
            .use("vue-style-loader"),
        config.module
            .rule("postcss")
            .oneOf("normal")
            .use("vue-style-loader"),
        config.module
            .rule("scss")
            .oneOf("vue-modules")
            .use("vue-style-loader"),
        config.module
            .rule("scss")
            .oneOf("vue")
            .use("vue-style-loader"),
        config.module
            .rule("scss")
            .oneOf("normal-modules")
            .use("vue-style-loader"),
        config.module
            .rule("scss")
            .oneOf("normal")
            .use("vue-style-loader"),
    ];
    configs.forEach(c =>
        c.tap(options => {
            if (options === undefined) return undefined;

            options.shadowMode = true;
            return options;
        })
    );
}

let baseUrl = process.env.VUE_APP_BASE_URL;
if (baseUrl === undefined) baseUrl = "/";

process.env.VUE_APP_BASE_URL = baseUrl;
console.log("Base url:", baseUrl);

if (process.env.VUE_APP_API_URL === undefined) process.env.VUE_APP_API_URL = urlJoin(baseUrl, "api/");
console.log("Api url:", process.env.VUE_APP_API_URL);

module.exports = {
    publicPath: baseUrl,

    css: {
        extract: process.env.VUE_APP_MODE === "production" && !process.env.VUE_APP_SHADOW_MODE,
    },

    configureWebpack: {
        resolve: {
            alias: {
                styles: path.resolve(__dirname, "src/styles/"),
                "google-libphonenumber": path.resolve(__dirname, "src/libphonenumber-stub.js"),
            },
        },
        mode: process.env.VUE_APP_MODE !== "development" ? "production" : "development",
    },

    chainWebpack: config => {
        if (process.env.VUE_APP_SHADOW_MODE) {
            enableShadowCss(config);
        }
    },
};
